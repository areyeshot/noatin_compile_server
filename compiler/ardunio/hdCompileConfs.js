module.exports = {
    "2": {
        "postfix": "hex",
        "arch": "arduino:avr:uno",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "3": {
        "postfix": "hex",
        "arch": "arduino:avr:nano:cpu=atmega168",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "4": {
        "postfix": "hex",
        "arch": "arduino:avr:mini",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "5": {
        "postfix": "hex",
        "arch": "arduino:avr:unowifi",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "6": {
        "postfix": "hex",
        "arch": "arduino:avr:nano:cpu=atmega168",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "7": {
        "postfix": "hex",
        "arch": "arduino:avr:yun",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "9": {
        "postfix": "bin",
        "arch": "esp8266:esp8266:nodemcuv2:CpuFrequency=80,UploadSpeed=115200,FlashSize=4M3M",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.esptool.path="{thirdPartyPath}esp8266/esp8266/tools/esptool" -prefs=runtime.tools.xtensa-lx106-elf-gcc.path="{thirdPartyPath}esp8266/esp8266/tools/xtensa-lx106-elf-gcc" -prefs=runtime.tools.mkspiffs.path="{thirdPartyPath}esp8266/esp8266/tools/mkspiffs"'
    },
    "12": {
        "postfix": "bin",
        "arch": "espressif:esp32:esp32:FlashFreq=80,UploadSpeed=921600,DebugLevel=none",
        "prefs": '-prefs=build.warn_data_percentage=75'
    },
    "13": {
        "postfix": "bin",
        "arch": "esp8266:esp8266:d1:CpuFrequency=80,UploadSpeed=921600,FlashSize=4M3M",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.esptool.path="{thirdPartyPath}esp8266/esp8266/tools/esptool" -prefs=runtime.tools.xtensa-lx106-elf-gcc.path="{thirdPartyPath}esp8266/esp8266/tools/xtensa-lx106-elf-gcc" -prefs=runtime.tools.mkspiffs.path="{thirdPartyPath}esp8266/esp8266/tools/mkspiffs"'
    },
    "14": {
        "postfix": "bin",
        "arch": "esp8266:esp8266:d1_mini:CpuFrequency=80,UploadSpeed=921600,FlashSize=4M3M",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.esptool.path="{thirdPartyPath}esp8266/esp8266/tools/esptool" -prefs=runtime.tools.xtensa-lx106-elf-gcc.path="{thirdPartyPath}esp8266/esp8266/tools/xtensa-lx106-elf-gcc" -prefs=runtime.tools.mkspiffs.path="{thirdPartyPath}esp8266/esp8266/tools/mkspiffs"'
    },
    "15": {
        "postfix": "bin",
        "arch": "espressif:esp32:esp32:FlashFreq=80,UploadSpeed=921600,DebugLevel=none",
        "prefs": '-prefs=build.warn_data_percentage=75'
    },
    "27": {
        "postfix": "hex",
        "arch": "arduino:avr:noatinunowifi",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "28": {
        "postfix": "hex",
        "arch": "arduino:avr:mega:cpu=atmega2560",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "29": {
        "postfix": "hex",
        "arch": "arduino:avr:mega:cpu=atmega2560",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "30": {
        "postfix": "hex",
        "arch": "arduino:avr:nano:cpu=atmega328",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.avr-gcc.path="{arduinoPath}avr" -prefs=runtime.tools.arduinoOTA.path="{arduinoPath}avr" -prefs=runtime.tools.avrdude.path="{arduinoPath}avr"'
    },
    "31": {
        "postfix": "bin",
        "arch": "STM32:STM32F4:generic_f407v:usb_cfg=usb_serial,upload_method=QCarUploadMethod,opt=osstd",
        "prefs": '-prefs=build.warn_data_percentage=75 -prefs=runtime.tools.arm-none-eabi-gcc.path="/usr/bin/gcc-arm-none-eabi/"'
    },
}; 