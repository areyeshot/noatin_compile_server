#!/bin/bash
mkdir logs
now(){
  date "+%Y-%m-%d-%H.%M.%S.%N"
}
while true
do
  echo "start" $(now)
  nohup node index.js > logs/log$(now).log
done