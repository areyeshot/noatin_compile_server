//这是配置模版，启动加载config-local
const httpPort = 8090;
const hdcompiler = {
	"ardunio": {
		'command': '/root/noatin/arduinos/arduino-1.8.9/arduino-builder',
		'hardware': '-hardware "/root/noatin/arduinos/arduino-1.8.9/hardware" -hardware "/root/Arduino/hardware"',
		'builtinlibraries': '-built-in-libraries "/root/noatin/arduinos/arduino-1.8.9/libraries"',
		'libraries': '-libraries /root/noatin/arduinos/arduino-1.8.9/libraries -libraries /root/Arduino/libraries',
		'tools': '-tools "/root/noatin/arduinos/arduino-1.8.9/tools-builder" -tools "/root/noatin/arduinos/arduino-1.8.9/hardware/tools/avr"',
		'rootBuildPath': '/root/noatin/tmp/arduinoBuild', // must set
		// 'verbose': true,
		"arduinoPath": "/root/noatin/arduinos/arduino-1.8.9/hardware/tools/", //官方路径
		"thirdPartyPath": "/root/Arduino/hardware/" //第三方路径
	}
};

module.exports = {
	httpPort,
	hdcompiler
}