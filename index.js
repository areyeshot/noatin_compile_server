const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const compression = require('compression');
const helmet = require('helmet');
//const config = require('./config').config;
const config = require('./config-local');

const app = express();
app.use(helmet());
app.use(compression({
    level: 9
}));
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.text({
    limit: '50mb',
    extended: true
}));
app.set('trust proxy', 1) // trust first proxy

app.use(session({
    secret: 'MakeLouden.AREyeShot',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false,
        httpOnly: true,
        maxAge: 30 * 24 * 60 * 60 * 1000
    }
}));

for (let moduleName in config.hdcompiler) {
    let moduleConfig = config.hdcompiler[moduleName];
    let module = require("./compiler/" + moduleName);
    if (module.initApp) {
        module.initApp(moduleConfig, app);
    }
}

app.listen(config.httpPort, function () {
    console.log('Example app listening on port ' + config.httpPort);
});